import {AdminModule} from '@universis/ngx-admin';
import {NgModule} from '@angular/core';

@NgModule({
  imports: [AdminModule]
})
export class AdminWrapperModule {
}
